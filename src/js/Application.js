import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    this.emojis = emojis;
  }

  addBananas() {
    let pEl = document.createElement('p');
    document.getElementById('emojis').appendChild(pEl);
    let myArr = this.emojis.map(x => (x + this.banana));
    pEl.textContent = myArr;
    console.log(myArr);
  }
}
